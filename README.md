# read_plotSpec_Intermag
Perform the Fourier transform power spectrum analysis in a ground magnetic station of the Intermagnet network.


## License

[![GPL-3.0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)
