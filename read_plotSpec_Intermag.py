#!/usr/bin/python2
# coding=utf-8
# author: Jose P. Marchezi
import numpy as np
import os
import datetime
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scipy import signal
import glob
from analysisFunc import butter_bandpass, butter_bandpass_filter, fill_nan, dll_calc
from marxeDownloader import MarxeDownloader
import ftplib


matplotlib.rc('text', usetex=True) # setting to allow LaTex comands and symbols
matplotlib.rc('font', family='serif', size=12)


# parameters to set up, if necessary...
station = 'fcc' # station name
year = '2017'
month = '01'
# set the days that will be analysed, comma separated
days = ['26']

colorLim = [-1, 2] # set the colorbar limits


####
# directory of the data
path = os.getcwd() #'/home/jose/MEGA/Doutorado/Progs/plot_ULF/dados/'
dataDownlDir = path + '/dados/'
plotDir = path + '/plots/'

kpFilename = 'Kp_sep21_25.txt'
kp = np.loadtxt(dataDownlDir+kpFilename,delimiter=' ', skiprows=16, usecols=(2,2), unpack=True)
dllB, dllE, dllt = dll_calc(kp)

############  FTP   ###############333333

## Open the Intermagnet ftp and download the data

host = 'ftp.intermagnet.org' # ftp host name
user = 'imaginpe' # ftp user name
password = 'd@a^DGE' # ftp password
# Directory contain the data
working_directory = 'minute/variation/IAGA2002/'+year+'/'+month+'/'

listFilename = []
for d in days:
    listFilename.append(station+year+month+d+'vmin.min')

# filename = station+year+month+day+'vmin.min' # data filename

# Creating a instance of MarxéDownloader
mx = MarxeDownloader(host, user, password)

# Connecting
mx.connect()

# Set downloaded data directory
mx.set_output_directory(dataDownlDir)

# Set FTP directory to download
mx.set_directory(working_directory)

# Download the file
# mx.download_one_data(filename)

# Download Many Data
mx.download_many_data(listFilename)

######### Reading the files and perform the analysis ##################

stations = [station]
varNames = []
for s in stations:
    filen = s + '*'
    local_file = dataDownlDir + filen
    files_mag = glob.glob(local_file) # list the files
    files_mag.sort() # sort
    for f in files_mag:
        print ('opening -> ' + f[-19:])
        fil = open(f, 'r') # open the file
        data = fil.readlines()  # read the lines
        lat = data[4][24:30] # load latitude and longitude
        lon = data[5][24:30]
        sk = len(data) - 1440 # set how many lines of headers to skip
        fil.close()
        print ('loading the geomagnetic components...')
        # load the X, Y and Z components
        X, Y, Z = np.loadtxt(f, skiprows=sk, usecols=(3, 4, 5), unpack = True)
        # locate and replace gaps
        X[X == 99999.00] = 'nan'
        Y[Y == 99999.00] = 'nan'
        Z[Z == 99999.00] = 'nan'
        H = np.sqrt(X ** 2 + Y ** 2) # Calculate the H component
        Hn = []
        for i in range(0, 1440):
            Hn.append(H[i] - H[0]) # Extract the local background
        os.remove(f) # remove the file
        print ('filtering the Pcs....')
        # apply the filter for Pc 5
        bpFil = butter_bandpass_filter(Hn,1./600,1./150,1./60,order = 3)
        time_window = 30 # timestep for the fourier transform
        # window length
        window_length = int(np.fix(time_window * (60 / 60)))
        noverlap = int(np.fix(window_length / 2)) # overlap data
        nfft= window_length
        # Fourrier transforms
        print ('Fourier transforms ...')
        fbp, tbp, psd_bp = signal.spectrogram(bpFil, fs= 1. / 60, window='hamming',
                                       nperseg=nfft, noverlap=noverlap,
                                       nfft=nfft, axis=-1, mode='complex')
        date_list = [datetime.datetime(int(f[-16:-12]),int(f[-12:-10]),int(f[-10:-8]),0,0)
                     + datetime.timedelta(minutes=x) for x in range(0, 1440)]
        date_list2 = [datetime.datetime(int(f[-16:-12]),int(f[-12:-10]),int(f[-10:-8]),1,15)
                     + datetime.timedelta(minutes=x) for x in range(0, 1440, 15)]

        print('Ploting and saving the figures....')
        # Plots
        # Spectrogram
        fig = plt.figure(figsize=(20, 6))
        pl = fig.add_subplot(111)
        sp = pl.pcolormesh(date_list2[:-1], fbp * 1e3, (np.log10(abs(psd_bp))))
        plt.xlabel('time [UT]')
        plt.ylabel('Frequency (mHz)')
        plt.title('H-Component, %s-%s-%s' % (f[-16:-12], f[-12:-10], f[-10:-8]))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 25, 2)))
        plt.ylim(0, 8)
        # plt.xlim(min(tbp), max(tbp))
        fig_coord = [0.92, 0.12, 0.01, 0.755]
        cbar_ax = fig.add_axes(fig_coord)
        cbar = fig.colorbar(sp, cax=cbar_ax, boundaries=np.linspace(colorLim[0],colorLim[1],64),
                            ticks = np.linspace(colorLim[0],colorLim[1],6),
                            label = '$log_{10}[(nT)^2/Hz)]$')
        cbar.set_clim(colorLim[0],colorLim[1])
        plt.savefig(plotDir+'Spec'+f[-19:-8]+'.png')
        plt.close()
        # Filtered signal
        fig = plt.figure(figsize=(20, 6))
        pl = fig.add_subplot(111)
        plt.plot(date_list, bpFil)
        plt.xlabel('time [UT]')
        plt.ylabel('nT')
        plt.grid()
        plt.xlim(min(date_list), max(date_list))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 25, 2)))
        plt.title('Pc5, %s-%s-%s' % (f[-16:-12], f[-12:-10], f[-10:-8]))
        plt.savefig(plotDir+'Pc'+f[-19:-8]+'.png')
        plt.close()
        # Two
        fig = plt.figure(figsize=(20, 12))
        pl = fig.add_subplot(211)
        plt.plot(date_list, bpFil)
        plt.xlabel('time [UT]')
        plt.ylabel('nT')
        plt.grid()
        plt.xlim(min(date_list), max(date_list))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 25, 2)))
        plt.title('Pc5, %s-%s-%s' % (f[-16:-12], f[-12:-10], f[-10:-8]))

        pl = fig.add_subplot(212)
        sp = pl.pcolormesh(date_list2[:-1], fbp * 1e3, (np.log10(abs(psd_bp))))
        plt.xlabel('time [UT]')
        plt.ylabel('Frequency (mHz)')
        # plt.title('H-Component, %s-%s-%s' % (f[-16:-12], f[-12:-10], f[-10:-8]))
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        plt.gca().xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 25, 2)))
        plt.ylim(0, 8)
        # plt.xlim(min(tbp), max(tbp))
        fig_coord = [0.92, 0.12, 0.01, 0.33]
        cbar_ax = fig.add_axes(fig_coord)
        cbar = fig.colorbar(sp, cax=cbar_ax, boundaries=np.linspace(colorLim[0],colorLim[1],64),
                            ticks = np.linspace(colorLim[0],colorLim[1],6),
                            label = '$log_{10}[(nT)^2/Hz)]$')
        cbar.set_clim(colorLim[0],colorLim[1])
        plt.savefig(plotDir+'Two'+f[-19:-8]+'.png')

print('\n\nDone!')
